﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Linq;

namespace AnonimizacjaV2.Services
{
    public class DictionaryService 
    {
        private string sPath = Config.dictionaryFile;
        private ListBox dictionaryList;
        private string[] foo;
        private string line;

        public DictionaryService(ListBox dictionaryList, string sPath)
        {
            this.dictionaryList = dictionaryList;
            this.sPath = sPath;
        }

        public DictionaryService()
        {

        }

        public string[] loadItemsFromFile()
        {
            if (File.Exists(this.sPath))
            {
                string line;
                var file = new StreamReader(this.sPath);
                while ((line = file.ReadLine()) != null)
                {
                    this.dictionaryList.Items.Add(line);
                }
            }
            return this.dictionaryList.Items.OfType<string>().ToArray(); ;
        }

        public string[] loadArray()
        {
            if (File.Exists(this.sPath))
            {
                string line;
                var file = new StreamReader(this.sPath);
                int i = 0;
                while ((this.line = file.ReadLine()) != null)
                {
                    this.foo[i] = this.line;
                    i++;
                }
            }
            return this.foo;
        }
       

        public bool saveDictionary()
        {
            StreamWriter SaveFile = new StreamWriter(this.sPath);
            foreach (var item in this.dictionaryList.Items)
            {
                SaveFile.WriteLine(item.ToString());
            }
            SaveFile.Close();
            return true;
            
        }

        public void addItem(string inputValue)
        {
             this.dictionaryList.Items.Add(inputValue);
        }

        public void removeItem()
        {
            this.dictionaryList.Items.RemoveAt(this.dictionaryList.Items.IndexOf(this.dictionaryList.SelectedItem));
        }
    }
}
