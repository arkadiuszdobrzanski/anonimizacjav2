﻿using System.Collections.Generic;
using System.IO;
using System.Collections;
using Tesseract;
using System;
using System.Diagnostics;

namespace AnonimizacjaV2.Services
{

    class PdfOcrService
    {
        private string text;

        public void OCRtoPDF(string filePath)
        {
            using (IResultRenderer renderer = Tesseract.PdfResultRenderer.CreatePdfRenderer(@""+ Path.GetDirectoryName(filePath) +"/"+ Path.GetFileNameWithoutExtension(filePath), @"C:\Users\arked\source\repos\anonimizacjav2\WpfApp1\bin\Debug\netcoreapp3.1\tessdata\", false))
            {
                using (renderer.BeginDocument("Path.GetFileNameWithoutExtension(filePath)"))
                {
                    string configurationFilePath = @"C:\Users\arked\source\repos\anonimizacjav2\WpfApp1\bin\Debug\netcoreapp3.1\tessdata";
                    string configfile = Path.Combine(@"C:\Users\arked\source\repos\anonimizacjav2\WpfApp1\bin\Debug\netcoreapp3.1\tessdata", "pdf");
                    using (TesseractEngine engine = new TesseractEngine(configurationFilePath, "pol", EngineMode.TesseractAndLstm, configfile))
                    {
                        using (Pix img = Pix.LoadFromFile(filePath))
                        {
                            using (var page = engine.Process(img, "Path.GetFileNameWithoutExtension(filePath)"))
                            {
                                renderer.AddPage(page);
                            }
                        }
                    }
                }
            }
        }

    }
}
