﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Leaf.xNet;

namespace AnonimizacjaV2.Services
{
    class LicenceService
    {
        private string licenceKey;
        private int members_limit;
        public int port;
        public string auth, url, response;
        private string baseURL = "https://anonimizacja.weblider24.pl/api";
        HttpRequest request = new HttpRequest();

        //important
        private void updateRequest()
        {
            this.request = new HttpRequest();
            this.request.AddHeader("Authorization", "Basic " + this.auth);
            this.request.AddHeader("Accept", "application/json");
            this.request.AddHeader("content-type", "application/json");
            this.request.IgnoreProtocolErrors = true;
        }

        public int getMembersLimit()
        {
            return this.members_limit;
        }

        public bool checkLicence(string licenceKey)
        {
            updateRequest();
            this.url = baseURL + "/checkLicence/"+licenceKey;
            this.response = request.Get(this.url).ToString();
            if (this.response.Contains("id"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
