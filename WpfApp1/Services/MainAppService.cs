﻿using System;
using System.Collections.Generic;
using System.Text;
using iText.Kernel.Pdf;
using iText.PdfCleanup.Autosweep;
using System.Text.RegularExpressions;
using iText.Kernel.Colors;
using iText.Layout;
using iText.IO.Image;
using iText.Layout.Element;
using System.Windows.Forms;
using iText.Kernel.Geom;

namespace AnonimizacjaV2.Services
{
    class MainAppService
    {
        private string imageFilePath;
        private string imagePosition;
        public Rectangle pageSize;
        public void execute(List<string> Arrayka, bool isImage)
        {
            PdfDocument pdf = new PdfDocument(new PdfReader(@"" + Config.filePath), new PdfWriter(@"" + Config.filePath + "_filled.pdf"));
            if (isImage)
            {
                this.insertImage(pdf);
            }
            foreach (var element in Arrayka)
            {
                ICleanupStrategy cleanupStrategy1 = new RegexBasedCleanupStrategy(new Regex(@"" + element + "", RegexOptions.IgnoreCase)).SetRedactionColor(ColorConstants.BLACK);
                PdfAutoSweep autoSweep = new PdfAutoSweep(cleanupStrategy1);
                autoSweep.CleanUp(pdf);
            }
            pdf.Close();
        }

        public string openFile()
        {
            OpenFileDialog choofdlog = new OpenFileDialog();
            choofdlog.Filter = "Plik obrazu (*.jpg)|*.jpg|Plik obrazu (*.png)|*.png";
            choofdlog.FilterIndex = 1;
            choofdlog.Multiselect = false;

            if (choofdlog.ShowDialog() == DialogResult.OK)
            {
                this.imageFilePath = choofdlog.FileName;
            }
            return this.imageFilePath;
        }

        public void insertImage(PdfDocument pdf)
        {

            int numberOfPages = pdf.GetNumberOfPages();

            Document document = new Document(pdf);
            ImageData imageData = ImageDataFactory.Create(this.imageFilePath);

            for (int i = 1; i <= numberOfPages; i++)
            {
                this.pageSize = pdf.GetPage(i).GetPageSize();

                switch (this.imagePosition)
                {
                    case "Lewy górny":
                        this.imageAddHelper(numberOfPages, imageData, 25, 25, document, pdf, i);
                        break;
                    case "Lewy dolny":
                        //
                        this.imageAddHelper(numberOfPages, imageData, 25, 25, document, pdf, i);
                        break;
                    case "Prawy górny":
                        this.imageAddHelper(numberOfPages, imageData, 25, 25, document, pdf, i);
                        break;
                    case "Prawy dolny":
                        this.imageAddHelper(numberOfPages, imageData, 25, 25, document, pdf, i);
                        break;
                    default:
                        this.imageAddHelper(numberOfPages, imageData, 25, 25, document, pdf, i);
                        break;
                }
            }

        }

        public void setImagePosition(string imagePosition)
        {
            this.imagePosition = imagePosition;
        }

        public void imageAddHelper(int numberOfPages, ImageData imageData, float x, float y, Document document, PdfDocument pdf, int i)
        {
            Image image = new Image(imageData).ScaleAbsolute(100, 200).SetFixedPosition(i, x, y);
            document.Add(image);
        }

    }
}
