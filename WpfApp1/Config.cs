﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnonimizacjaV2
{
    public class Config
    {
        public static String fileName { get; set; }
        public static String filePath { get; set; }
        public static String directory { get; set; }
        public static String citiesFile = "cities.txt";
        public static String dictionaryFile = "dictionary.txt";

    }
}
