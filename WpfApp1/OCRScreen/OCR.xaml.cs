﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AnonimizacjaV2.OCRScreen
{
    /// <summary>
    /// Logika interakcji dla klasy OCR.xaml
    /// </summary>
    public partial class OCR : Window
    {
        private Services.PdfOcrService pdfOcrService;
        private string file;
        public OCR()
        {
            InitializeComponent();
            this.pdfOcrService = new Services.PdfOcrService();
        }

        private void addToDictionary_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image files (*.png;*.jpeg)|*.png;*.jpeg|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
                image_path.Content = openFileDialog.FileName;

            this.file = openFileDialog.FileName;

        }

        private void saveDictionary_Click(object sender, RoutedEventArgs e)
        {
            this.pdfOcrService.OCRtoPDF(this.file);

        }
    }
}
