﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using iText.PdfCleanup.Autosweep;
using iText.Kernel.Pdf;
using System.Text.RegularExpressions;
using iText.Kernel.Colors;

namespace AnonimizacjaV2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Dictionary.CitiesDictionary citiesArray;
        private Services.MainAppService MainAppService;
        public List<string> Arrayka = new List<string> { "" };
        private bool isImage = false;
        public MainWindow()
        {
            InitializeComponent();
            this.MainAppService = new Services.MainAppService();
            this.citiesArray = new Dictionary.CitiesDictionary();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            PdfView.searchValues(this.searchValue.Text);
        }

        private void openDictionary_Click(object sender, RoutedEventArgs e)
        {
            Dictionary.DictionaryMainWindow dicWindow = new Dictionary.DictionaryMainWindow();
            dicWindow.Show();
        }

        private void chooseFile_Click(object sender, RoutedEventArgs e)
        {
            this.imageFilePath.Content = this.MainAppService.openFile();
        }

        private void execute_Click(object sender, RoutedEventArgs e)
        {
          
            if(this.hideDic.IsChecked == true)
            {
                Services.DictionaryService dictionaryService = new Services.DictionaryService();
                foreach (var foo in dictionaryService.loadArray())
                {
                    this.Arrayka.Add(foo.ToString().TrimEnd(' '));
                }
            }

            if (!string.IsNullOrWhiteSpace(this.searchValue.Text))
            {
                this.Arrayka.Add(this.searchValue.Text);
            }

            if(this.hidePesel.IsChecked == true)
            {
                
                this.Arrayka.Add("[0-9]{11}");
            }

            //uzycie obrazka
            if(this.useImage.IsChecked == true)
            {
                string imagePosition = this.imagePosition.SelectedItem.ToString();
                this.MainAppService.setImagePosition(imagePosition);
                this.isImage = true;
            }

            if (this.hideCity.IsChecked == true)
            {
                foreach( var foo in this.citiesArray.getCitites())
                {
                    this.Arrayka.Add(foo.ToString().TrimEnd(' '));
                }
            }
            
            //wywołanie anonimizacji
            this.MainAppService.execute(this.Arrayka, this.isImage);
           

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            
            OCRScreen.OCR ocrWindow = new OCRScreen.OCR();
            ocrWindow.Show();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            PdfView pdfView = new PdfView();
        }
    }
}