﻿using System;
using System.Windows.Forms;
using PdfiumViewer;


namespace AnonimizacjaV2
{
    public partial class PdfView : UserControl
    {
        private static PdfSearchManager _searchManager;
        private bool _findDirty;
        public String filePath, directory, fileName;
        public Config config;
        public PdfView()
        {
            InitializeComponent();
            
            _searchManager = new PdfSearchManager(pdfViewer1.Renderer);
            _searchManager.HighlightAllMatches = true;
            Disposed += (s, e) => pdfViewer1.Document?.Dispose();
            var args = Environment.GetCommandLineArgs();
            if (args.Length > 1)
            {
                pdfViewer1.Document?.Dispose();
                pdfViewer1.Document = OpenDocument(args[1]);
            }
            else
            {
                OpenFile();
            }
        }

        public static void searchValues(string searchValue)
        {
            if (!_searchManager.Search(searchValue))
            {
                MessageBox.Show("Brak takiego słowa!");
                return;
            }
        }

        private void pdfViewer1_Load(object sender, EventArgs e)
        {

        }

        private PdfiumViewer.PdfDocument OpenDocument(string fileName)
        {
            try
            {
                return PdfiumViewer.PdfDocument.Load(this, fileName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        public void OpenFile()
        {
            using (var form = new OpenFileDialog())
            {
                form.Filter = "PDF Files (*.pdf)|*.pdf|All Files (*.*)|*.*";
                form.RestoreDirectory = true;
                form.Title = "Otwórz plik PDF";

                if (form.ShowDialog(this) != DialogResult.OK)
                {
                    Dispose();
                    return;
                }
                Config.directory = form.InitialDirectory;
                Config.fileName = form.SafeFileName;
                Config.filePath = form.InitialDirectory + form.FileName;
                pdfViewer1.Document?.Dispose();
                pdfViewer1.Document = OpenDocument(form.FileName);
                this.Refresh();
            }
        }

       
    }
}
