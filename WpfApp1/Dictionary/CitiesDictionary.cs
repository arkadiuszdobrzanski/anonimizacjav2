﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.IO;

namespace AnonimizacjaV2.Dictionary
{
    public class CitiesDictionary
    {
        public string sPath = Config.citiesFile;
        private string line;
        private string[] foo;
    
        public CitiesDictionary()
        {
            string[] arrayCities = new string[953];
            this.foo = arrayCities;
        }


        public string[] getCitites()
        {
            if (File.Exists(this.sPath))
            {
                var file = new StreamReader(this.sPath);
                int i = 0;
                while ((this.line = file.ReadLine()) != null)
                {
                    this.foo[i] = this.line;
                    i++;
                }
            }
            return this.foo;
        }
    }
}
