﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AnonimizacjaV2.Dictionary
{
    /// <summary>
    /// Logika interakcji dla klasy DictionaryMainWindow.xaml
    /// </summary>
    public partial class DictionaryMainWindow : Window
    {
        public string sPath = Config.dictionaryFile;
        public Services.DictionaryService dictionaryService;
        public string line;
        public DictionaryMainWindow()
        {
            InitializeComponent();
            this.dictionaryService = new Services.DictionaryService(this.dictionary, this.sPath);
            this.dictionaryService.loadItemsFromFile();
        }


        private void addToDictionary_Click(object sender, RoutedEventArgs e)
        {
            this.dictionaryService.addItem(d_inputValue.Text);
        }

        private void deleteFromDictionary_Click(object sender, RoutedEventArgs e)
        {
            this.dictionary.Items.RemoveAt(this.dictionary.Items.IndexOf(this.dictionary.SelectedItem));
        }

        private void saveDictionary_Click(object sender, RoutedEventArgs e)
        {
            if (this.dictionaryService.saveDictionary())
            {
                MessageBox.Show("Słownik zapisany");
            }
        }
    }
}
